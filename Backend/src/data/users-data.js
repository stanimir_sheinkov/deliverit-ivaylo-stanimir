import pool from "./pool.js";
import bcrypt from "bcrypt";

/**
 * CHECK IF USER EXISTS
 * @param {string} email
 * @returns true or false
 */
export const checkIfUserExists = async (email) => {
  return !!(await getSingleUserByEmail(email));
};

/**
 * GET ALL USERS
 * @returns all users full info
 */
const getAllUsers = async () => {
  const sql = `
      SELECT * 
      FROM users
      `;

  return await pool.query(sql);
};

/**
 * GET ALL ACTIVE USERS
 * @returns all active users full info
 */
const getAllActiveUsers = async () => {
  const sql = `
      SELECT * 
      FROM users
      WHERE is_deleted = 0
      `;

  return await pool.query(sql);
};

/**
 * GET SINGLE USER BY EMAIL
 * @param {string} email
 * @returns the selected users full info.
 */
const getSingleUserByEmail = async (email) => {
  const sql = `
      SELECT idusers, first_name, last_name, email, role, country, city, street, password, is_deleted, created_on
      FROM users
      WHERE email = ? AND is_deleted = 0
      `;

  const result = await pool.query(sql, [email]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * GET SINGLE USER BY ID
 * @param {number} id
 * @returns the selected users full info.
 */
const getSingleUserById = async (id) => {
  const sql = `
      SELECT  first_name, last_name, email, role,    country, city, street, password, is_deleted, created_on
      FROM users
      WHERE idusers = ? AND is_deleted = 0
      `;

  const result = await pool.query(sql, [id]);

  if (result[0]) {
    return result[0];
  }

  return null;
};

/**
 * GET ALL DELETED USERS
 * @returns
 */
const getAllDeletedUsers = async () => {
  const sql = `
      SELECT *
      FROM users
      WHERE is_deleted = 1`;
  return await pool.query(sql);
};

/**
 * REGISTER NEW USER
 * @param {string} user
 * @returns
 */
const registerNewUser = async (user) => {
  const hashPassword = await bcrypt.hash(user.password, 10);

  const sql = `
    INSERT INTO users (first_name, last_name, email, city, country, street, password)
    VALUES (?, ?, ?, ?, ?, ?, ?)
    `;
  return await pool.query(sql, [
    user.first_name,
    user.last_name,
    user.email,
    user.city,
    user.country,
    user.street,
    hashPassword,
  ]);
};

/**
 * BLACKLIST OLD TOKEN
 * @param {string} token
 * @returns
 */
const saveOldToken = async (token) => {
  const sqlQuery = `INSERT INTO tokens(token) VALUES (?)`;

  try {
    return await pool.query(sqlQuery, [token]);
  } catch (err) {
    console.log(err.message);
    return null;
  }
};

/**
 * CHECK FOR TOKEN IN BLACKLIST
 * @param {string} token
 * @returns
 */
const checkOldTokens = async (token) => {
  const sqlQuery = `SELECT token FROM tokens WHERE token = ?`;

  try {
    const validToken = await pool.query(sqlQuery, [token]);
    return validToken.length ? true : false;
  } catch (err) {
    console.log(err.message);
    return null;
  }
};

/**
 * UPDATE USER BY ID
 * @param {number} userId 
 * @param {object} newData 
 * @returns 
 */
export const updateUserById = async (userId, newData) => {
  const hashPassword = await bcrypt.hash(newData.password, 10);

  const sqlQuery = `
      UPDATE users  
      SET first_name = ?, last_name = ?, email = ?, role = ?, country = ?, city = ?, street = ?, password = ?, is_deleted = ?
      WHERE idusers = ? 
      `;

  const updatedUser = await pool.query(sqlQuery, [
    newData.first_name,
    newData.last_name,
    newData.email,
    newData.role,
    newData.country,
    newData.city,
    newData.street,
    hashPassword,
    newData.is_deleted,
    userId,
  ]);

  const result = await getSingleUserById(userId);

  if (result) {
    return {
      message: "User updated",
    };
  }
  return null;
};

export default {
  checkIfUserExists,
  getAllUsers,
  getAllActiveUsers,
  getSingleUserByEmail,
  getSingleUserById,
  getAllDeletedUsers,
  registerNewUser,
  saveOldToken,
  checkOldTokens,
  updateUserById,
};
