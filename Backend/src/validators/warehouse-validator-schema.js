import { check } from 'express-validator';

export const warehouseValidatorSchema = [
    // Validations for city
    
    check('city')
    .isLength({
        min: 3,
        max:20
    })
    .withMessage("City should be between 3 & 20 characters")
    .isAscii()
    .withMessage("Invalid city"),

    // Validations for country

    check('country')
    .isLength({
        min: 3,
        max:20
    })
    .withMessage("Country should be between 3 & 20 characters")
    .isAscii()
    .withMessage("Invalid country"),

    // Validations for street

    check('street')
    .isLength({
        min:5,
        max:30
    })
    .withMessage("Address should be between 5 & 20 characters")
    .isAscii()
    .withMessage("Invalid Address")
]
