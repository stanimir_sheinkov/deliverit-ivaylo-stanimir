// import { check } from 'express-validator';
import checkAPIs from 'express-validator'
const { check, validationResult} = checkAPIs;
export const countryValidatorSchema = [
    // Validations for country
    
    check('country')
    .isLength({
        min: 3,
        max:20
    })
    .withMessage("Country should be between 3 & 20 characters")
    .isAscii()
    .withMessage("Invalid country")
]
