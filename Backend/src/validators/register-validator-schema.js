import { check } from 'express-validator';

export const registerValidatorSchema = [
    check("first_name")
    .isLength({
        min:3,
        max:20
    })
    .withMessage("First name should be between 3 & 20 characters!"),

    check("last_name")
    .isLength({
        min:3,
        max:20
    })
    .withMessage("Last name should be between 3 & 20 characters!"),

    check("email")
    .isEmail()
    .withMessage("Invalid email"),

    check ("country")
    .isLength({
        min:3,
        max:20
    })
    .withMessage("Country's name should be between 3 & 20 characters!"),

    check("city")
    .isLength({
        min:3,
        max:20
    })
    .withMessage("City's name should be between 3 & 20 characters!"),

    check("street")
    .isLength({
        min:5,
        max:30
    })
    .withMessage("Address must be between 5 & 20 characters!"),

    check("password")
    .isLength({
        min:5,
        max:20
    })
    .withMessage("Password must be between 5 & 20 characters!")
]