export default {
    first_name: (value) => typeof value === 'string' && value.length > 0 && value.length < 30,
    last_namae: (value) => typeof value === 'string' && value.length > 0 && value.length < 30,
    email: (value) => typeof value === 'string' && value.length > 5 && value.length < 30,
    city: (value) => typeof value === 'string' && value.length > 0 && value.length < 30,
    country: (value) => typeof value === 'string' && value.length > 0 && value.length < 20,
    street: (value) => typeof value === 'string' && value.length > 0 && value.length < 50,
    password: (value) => typeof value === 'string' && value.length > 5 && value.length < 20,
};
