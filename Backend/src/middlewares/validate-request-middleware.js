// import { validationResult } from "express-validator";
import checkAPIs from 'express-validator'
const { check, validationResult} = checkAPIs;
export function validateRequestSchema(schema) {
        
    const result = [...schema, (req,res,next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json({
                errors: errors.array().map(err => ({
                    field: err.param,
                    message: err.msg,
                }))
            });
        }
        next();
    }]
    return result;
}

