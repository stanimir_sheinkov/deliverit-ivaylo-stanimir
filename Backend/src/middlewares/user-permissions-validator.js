import express from "express";
import userRoles from "../common/user-roles.js";

/**
 * Allow only members of specific group to continue
 * @param { boolean } role Group number who have permission to use this resource
 */
export const checkUserPermission = (role) => {
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  return async (req, res, next) => {
    if (role !== req.user.role) {
      return res
        .status(403)
        .send({ message: "You don't have permission to use this!" });
    }

    next();
  };
};

/**
 * Allow only user who own this resource or admin to continue based on req.params
 * @param {express.Request} req
 * @param {express.Response} res
 * @param {express.NextFunction} next
 */
export const rightUserOrAdmin = (firstComp, SecondComp) => (req, res, next) => {
  const fParams = firstComp.split(".");
  const sParams = SecondComp.split(".");

  const firstTarget = req[fParams[0]][fParams[1]];
  const secondTarget = req[sParams[0]][sParams[1]];

  if (
    +firstTarget !== +secondTarget &&
    +req.user.group !== userRoles.EMPLOYEE
  ) {
    return res
      .status(401)
      .send({ message: `You're not authorized to do that!` });
  }

  next();
};
