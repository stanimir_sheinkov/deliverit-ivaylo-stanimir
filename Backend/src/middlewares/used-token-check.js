import usersData from "../data/users-data.js";

export const oldTokenCheck = async (req, res, next) => {
  const myToken = req.headers.authorization.replace("Bearer ", "");

  if (await usersData.checkOldTokens(myToken)) {
    console.log("User tried to use a blacklisted token!");
    return res.status(401).send({ message: "Used token. Create a new one." });
  }
  return next();
};
