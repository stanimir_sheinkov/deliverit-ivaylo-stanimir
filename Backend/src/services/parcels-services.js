import { shipmentMaxWeight } from "../common/constants.js";
import parcelsData from "../data/parcels-data.js";
import shipmentsData from "../data/shipments-data.js";
import parcelsErrors from "../errors/parcels-errors.js";
import shipmentsErrors from "../errors/shipments-errors.js";

/**
 * GET ALL PARCELS
 * @returns all users full info
 */
const getParcels = async () => {
  const result = await parcelsData.getAllParcels();

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET SINGLE PARCEL INFO BY ID
 * @param {number} idparcels
 * @returns {object} containing the full info of the selected parcel
 */
const getParcel = async (idparcels) => {
  const result = await parcelsData.getSingleParcel(idparcels);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ALL PARCELS OF USER BY iduser
 * @param {number} id
 * @returns a list with all user's parcel, with their full info
 */
const getParcelByUser = async (id) => {
  const result = await parcelsData.getParcelsByUserId(id);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_FOR_USER_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ALL PARCELS FROM CATEGORY
 * @param {string} category
 * @returns a list with all parcels from this category
 */
const getParcelsFromCategory = async (category) => {
  const result = await parcelsData.getCategoryParcels(category);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * UPDATE PARCEL BY ID. 
 * Takes shipment current weight. 
 * Checks if adding the parcel will exceed weight limit.
 * Updates the parcel information. 
 * Updates the weight of the shipment.
 * @param {number} idparcel
 * @param {object} body
 * @returns
 */
const updateParcelInfo = async (idparcel, body) => {
  const shipmentWeight = await shipmentsData.getShipmentWeight(
    body.idshipments
  );

  
  let newShipmentWeight = +shipmentWeight + +body.weight;
  // console.log(body.weight);
  // console.log(shipmentWeight[0]);
  if (newShipmentWeight > shipmentMaxWeight) {
    return {
      error: shipmentsErrors.SHIPMENT_FULL,
    };
  }

  const updatedInfo = await parcelsData.updateParcelInfo(idparcel, body);

  if (!updatedInfo) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  const updatedShipmentWeight = await shipmentsData.updateShipmentWeightById(
    body.idshipments,
    newShipmentWeight
  );

  if (!updatedShipmentWeight) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};

/**
 * DELETE PARCEL BY ID
 * @param {number} idparcel
 * @param {object} body
 * @returns
 */
const deleteParcelById = async (idparcel, del) => {
  const updatedInfo = await parcelsData.deleteParcel(idparcel, del);

  if (!updatedInfo) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};

/**
 * GET ALL PARCELS BY WEIGHT
 * @param {number} weight
 * @returns all parcels with the selected weight
 */
const getParcelsWithWeight = async (weight) => {
  const result = await parcelsData.getParcelsByWeight(weight);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_WITH_WEIGHT,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ALL PARCELS TO DESTINATION WAREHOUSE
 * @param {number} warehouse
 * @returns
 */
const getParcelsToDefaultWarehouse = async (warehouse) => {
  const result = await parcelsData.getParcelsByDefaultWarehouse(warehouse);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_TO_DEFAULT_WH,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ALL PARCELS IN SHIPMENT
 * @param {number} shipmentid
 * @returns
 */
const getParcelsFromShipment = async (shipmentid) => {
  const result = await parcelsData.getParcelsByShipment(shipmentid);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_TO_DEFAULT_WH,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET PARCELS BY IS_DELETED FIELD
 * @param {number} isdeleted
 * @returns
 */
const getParcelsByDelete = async (isdeleted) => {
  const result = await parcelsData.getParcelsByDel(isdeleted);

  if (result.length === 0) {
    return {
      error: parcelsErrors.NO_PARCELS_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

const newParcel = async (parcel) => {
  if (!parcel) {
    return {
      error: parcelsErrors.WRONG_PARCEL_INPUT,
      data: null,
    };
  }

  const createdShipment = await parcelsData.createParcel(parcel);

  return {
    error: null,
    data: { createdShipment },
  };
};

/**
 * UPDATE PARCEL STATUS BY ID_SHIPMENT
 */
export const updateParcelsShipmentStatus = async (id_shipment) => {
  const updatedInfo = await parcelsData.updateParcelsStatus(id_shipment);

  if (!updatedInfo) {
    return {
      error: shipmentsErrors.SHIPMENT_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: updatedInfo,
  };
};

export default {
  getParcels,
  getParcel,
  getParcelByUser,
  getParcelsFromCategory,
  updateParcelInfo,
  deleteParcelById,
  getParcelsWithWeight,
  getParcelsToDefaultWarehouse,
  getParcelsFromShipment,
  getParcelsByDelete,
  newParcel,
  // updateParcelsShipmentStatus,
};
