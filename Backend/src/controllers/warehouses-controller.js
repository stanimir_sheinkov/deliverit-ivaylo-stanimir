import express from "express";
import warehousesServices from "../services/warehouses-services.js";
import warehousesErrors from "../errors/warehouses-errors.js";

export const warehousesRouter = express.Router();

warehousesRouter
  /**
   * Get all warehouses
   */
  .get("/", async (req, res) => {
    const allWarehouses = await warehousesServices.getAllWarehouses();
    res.status(200).send(allWarehouses);
  })

  /**
   * Get warehouses by street
   */
  .get("/street/:warehouse", async (req, res) => {
    const { warehouse } = req.params;
    const { error, data } = await warehousesServices.getSingleWarehouse(
      warehouse
    );

    if (error === warehousesErrors.WAREHOUSE_NOT_FOUND) {
      return res.status(409).json({
        message: "Warehouse not found",
      });
    } else {
      return res.status(200).send(data);
    }
  })

  /**
   * Get warehouses by city
   */
  .get("/city/:city", async (req, res) => {
    const { city } = req.params;
    const data = await warehousesServices.getAllWarehousesByCity(city);
    res.status(200).send(data);
  })

  /**
   * Create a new warehouse
   */
  .post("/create", async (req, res) => {
    const { country, city, street } = req.body;
    const { error, data } = await warehousesServices.createWarehouse(
      country,
      city,
      street
    );

    if (error === warehousesErrors.WAREHOUSE_ALREADY_EXISTS) {
      return res.status(409).json({
        message: "Warehouse already exists",
      });
    } else {
      return res.status(200).send(data);
    }
  })

  /**
   * Update warehouse by id
   */

  .put("/update/:id", async (req, res) => {
    const { error, data } = await warehousesServices.updateWarehouse(
      req.params.id,
      req.body
    );

    if (error === warehousesErrors.WAREHOUSE_NOT_FOUND) {
      return res.status(404).send({
        message: `Warehouse not found!`,
      });
    }
    res.send(data);
  })

  /**
   * Get warehouse by id
   */
  .get("/:id", async (req, res) => {
    const { error, data } = await warehousesServices.getSingleWarehouseById(
      req.params.id
    );

    if (error === warehousesErrors.WAREHOUSE_NOT_FOUND) {
      return res.status(404).send({
        message: `No warehouse with such id found!`,
      });
    }

    res.status(200).send(data);
  });
