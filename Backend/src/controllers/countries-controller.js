import express from "express";
import countryServices from "../services/countries-services.js";
import countriesErrors from "../errors/countries-errors.js";
import { validateRequestSchema } from "../middlewares/validate-request-middleware.js";
import { countryValidatorSchema } from "../validators/countries-validator-schema.js";
export const countriesRouter = express.Router();

countriesRouter

  /**
   * Get all countries
   */
  .get("/", async (req, res) => {
    const countries = await countryServices.getCountries();
    res.status(200).send(countries);
  })

  /**
   * Get all active countries
   */
  .get("/active", async (req, res) => {
    const countries = await countryServices.getActiveCountries();
    res.status(200).send(countries);
  })
  .get("/:country", async (req, res) => {
    const result = await countryServices.getCountryByName(req.params.country);

    res.status(200).send(result);
  })

  /**
   * Create a new country.
   */
  .post(
    "/create/:country",
    validateRequestSchema(countryValidatorSchema),
    async (req, res) => {
      const { error, data } = await countryServices.createCountry(
        req.params.country
      );

      if (error === countriesErrors.ALREADY_EXISTS) {
        return res.status(409).json({
          message: `Country with this name already exists!`,
        });
      }
      res.json({
        message: `${req.params.country} added in countries!`,
      });
    }
  );
