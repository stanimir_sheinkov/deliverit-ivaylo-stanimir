import express from "express";
// import shipmentsData from "../data/shipments-data.js";
import shipmentsErrors from "../errors/shipments-errors.js";

import shipmentsServices from "../services/shipments-services.js";

export const shipmentsRouter = express.Router();

shipmentsRouter
  /**
   * Get all shipments with their full info.
   */
  .get("/", async (req, res) => {
    const { error, data } = await shipmentsServices.getShipments();

    if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
      return res.status(404).send({
        message: `No shipments found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get shipment info by id.
   */
  .get("/:id", async (req, res) => {
    const { error, data } = await shipmentsServices.getSingleShipment(
      req.params.id
    );

    if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
      return res.status(404).send({
        message: `No shipment with such id found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all shipments by from_warehouse
   */
  .get("/from_warehouse/:warehouse", async (req, res) => {
    const { error, data } = await shipmentsServices.getShipmentByWarehouse(
      req.params.warehouse
    );

    if (error === shipmentsErrors.NO_SHIPMENT_FROM_WAREHOUSE) {
      return res.status(404).send({
        message: `No shipment from this warehouse found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all shipments by to_warehouse
   */
  .get("/to_warehouse/:warehouse", async (req, res) => {
    const { error, data } = await shipmentsServices.getShipmentToWarehouse(
      req.params.warehouse
    );

    if (error === shipmentsErrors.NO_SHIPMENT_TO_WAREHOUSE) {
      return res.status(404).send({
        message: `No shipment to this warehouse found!`,
      });
    }

    res.status(200).send(data);
  })

  /**
   * Get all shipments by status
   */
  .get("/status/:status", async (req, res) => {
    const { error, data } = await shipmentsServices.getShipmentStatus(
      req.params.status
    );
    console.log(req.params.status);

    if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
      return res.status(404).send({
        message: `No shipment to this warehouse found!`,
      });
    }
    res.send(data);
  })

  /**
   * Get shipment status by id.
   */
     .get("/status/:id", async (req, res) => {
      const { error, data } = await shipmentsServices.getShipmentStatus(
        req.params.id
      );
  
      if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
        return res.status(404).send({
          message: `No shipment with such id found!`,
        });
      }
  
      res.status(200).send(data);
    })

  /**
   * Update shipment by id
   */
  .put("/update/:id", async (req, res) => {
    const { error, data } = await shipmentsServices.updateShipment(
      req.params.id,
      req.body
    );
    console.log(error);
    if (error === shipmentsErrors.SHIPMENT_NOT_FOUND) {
      return res.status(404).send({
        message: `Shipment not found!`,
      });
    }
    res.send(data);
  })

  /**
   * Create shipment
   */
  .post("/create", async (req, res) => {
    const { error, data } = await shipmentsServices.newShipment(req.body);

    if (error === shipmentsErrors.NO_SHIPMENT_DATA) {
      return res.status(400).json({
        message: `No shipment data input`,
      });
    }

    res.status(201).send({
      message: `Shipment created`,
    });
  });

  