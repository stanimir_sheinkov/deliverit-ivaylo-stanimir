import EditWarehouseForm from "../Components/EditForms/EditWarehouseForm";
import EditUserForm from "../Components/EditForms/EditUsersForm";
import EditShipmentForm from "../Components/EditForms/EditShipmentForm";
import EditParcelForm from "../Components/EditForms/EditParcelForm";
import EditParcelFormCustomer from "../Components/EditForms/EditParcelFormCustomer";

export const employeeTableWarehouse = [
    {
      field: "",
      headerName: "",
      editable: false,
      sortable: false,
      filterable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return <EditWarehouseForm {...params.row} />;
      },
    },
    { field: "id", headerName: "ID", width: 100 },
    {
      field: "country",
      headerName: "Country",
      width: 150,
      editable: false,
    },
    {
      field: "city",
      headerName: "City",
      width: 150,
      editable: false,
    },
    {
      field: "address",
      headerName: "Address",
      width: 150,
      editable: false,
    },
  ];

  export const employeeTableUsers = [
    {
      field: "",
      headerName: "",
      editable: false,
      sortable: false,
      filterable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return <EditUserForm {...params.row} />;
      },
    },
    { field: "id", headerName: "ID", width: 96 },
    {
      field: "first_name",
      headerName: "Name",
      width: 150,
      editable: false,
    },
    {
      field: "last_name",
      headerName: "Family",
      width: 150,
      editable: false,
    },
    {
      field: "email",
      headerName: "Email",
      width: 150,
      editable: false,
    },
    {
      field: "role",
      headerName: "Employee",
      type: "boolean",
      width: 140,
      editable: true,
    },
    {
      field: "country",
      headerName: "Country",
      width: 150,
      editable: false,
    },
    {
      field: "city",
      headerName: "City",
      width: 150,
      editable: false,
    },
    {
      field: "street",
      headerName: "Street",
      width: 150,
      editable: false,
    },
    {
      field: "is_deleted",
      headerName: "Deleted",
      type: "boolean",
      width: 150,
      editable: false,
    },
  ];

export const employeeTableShipments = [
    {
      field: "",
      headerName: "",
      editable: false,
      sortable: false,
      filterable: false,
      width: 100,
      disableClickEventBubbling: true,
      renderCell: (params) => {
          return <EditShipmentForm {...params.row} />
        // return <EditShipmentForm{...params.row} />;
      },
    },
    { field: "id", headerName: "ID", width: 100 },
    {
      field: "status",
      type: "string",
      headerName: "Status",
      width: 150,
      editable: false,
    },
    {
      field: "from_idwarehouses",
      headerName: "From",
      width: 150,
      editable: false,
    },
    {
      field: "to_idwarehouses",
      headerName: "To",
      width: 150,
      editable: false,
    },
    {
      field: "departure",
      headerName: "Departure",
      type: 'dateTime',
      width: 200,
      editable: false,
    },
    {
      field: "arrival",
      headerName: "Arrival",
      type: 'dateTime',
      width: 200,
      editable: false,
    },
    {
      field: "created_on",
      headerName: "Created on",
      type: 'dateTime',
      width: 200,
      editable: false,
    },
  ];

  export const employeeTableParcels = [
    {
      field: "",
      headerName: "",
      editable: false,
      sortable: false,
      filterable: false,
      width: 120,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return <EditParcelForm {...params.row} />;
      },
    },
    { field: "id", headerName: "ID", width: 100 },
    {
      field: "default_idwarehouse",
      headerName: "Warehouse",
      width: 150,
      editable: false,
    },
    {
      field: "weight",
      headerName: "Weight",
      width: 130,
      editable: false,
    },
    {
      field: "category",
      headerName: "Category",
      width: 150,
      editable: false,
    },
    {
      field: "iduser",
      headerName: "User ID",
      type: "number",
      width: 130,
      editable: false,
    },
    {
      field: "toWarehouse",
      headerName: "To Address",
      type: "boolean",
      width: 170,
      editable: false,
    },
    {
      field: "idshipments",
      headerName: "Shipment ID",
      type: "number",
      width: 160,
      editable: false,
    },
    {
      field: "shipment_status",
      headerName: "Status",
      width: 120,
      editable: false,
    },
    {
      field: "is_deleted",
      headerName: "Deleted",
      type: "boolean",
      width: 130,
      editable: false,
    },
  ];

  export const customerTableWarehouse = [
    { field: "id", headerName: "ID", width: 100 },
    {
      field: "country",
      headerName: "Country",
      width: 150,
      editable: false,
    },
    {
      field: "city",
      headerName: "City",
      width: 150,
      editable: false,
    },
    {
      field: "address",
      headerName: "Address",
      width: 150,
      editable: false,
    },
  ];

  export const customerTableParcels = [
    {
      field: "",
      headerName: "",
      editable: false,
      sortable: false,
      filterable: false,
      width: 140,
      disableClickEventBubbling: true,
      renderCell: (params) => {
        return <EditParcelFormCustomer {...params.row} />;
      },
    },
    { field: "id", headerName: "ID", width: 100 },
    {
      field: "default_idwarehouse",
      headerName: "Warehouse",
      width: 150,
      editable: false,
    },
    {
      field: "weight",
      headerName: "Weight",
      width: 130,
      editable: false,
    },
    {
      field: "category",
      headerName: "Category",
      width: 150,
      editable: false,
    },
    // {
    //   field: "iduser",
    //   headerName: "User ID",
    //   type: "number",
    //   width: 130,
    //   editable: false,
    // },
    {
      field: "toWarehouse",
      headerName: "To Address",
      type: "boolean",
      width: 170,
      editable: false,
    },
    {
      field: "idshipments",
      headerName: "Shipment ID",
      type: "number",
      width: 160,
      editable: false,
    },
    {
      field: "shipment_status",
      headerName: "Status",
      width: 120,
      editable: false,
    },
    {
      field: "is_deleted",
      headerName: "Deleted",
      type: "boolean",
      width: 130,
      editable: false,
    },
  ];