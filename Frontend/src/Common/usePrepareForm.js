import { MenuItem, Select, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import { BootstrapInput } from '../Components/EditForms/EditParcelForm';
// const {formstate} = usePrepareForm({defaultFormState, defaultErrorState, initialFormConfig, onSubmit })
const usePrepareForm = ({ defaultFormState, defaultErrorState, initialFormConfig, onSubmit }) => {
    const [form, setForm] = useState(defaultFormState);
    const [errors, setErrors] = useState(defaultErrorState);

    const formConfig = initialFormConfig.map(({ key, title, type, validations}) => ({ key, type, validations, placeholder: title, label: title, variant: 'outlined' }))

    const handleSubmit = (event) => {
        event.preventDefault();
        if (!!Object.values(errors).length) {
            onSubmit();
        }
    };

    const handleChange = (event, key) => {
        setForm({ ...form, [key]: event.target.value });
    };

    const validateInput = (value, validations, key) => {
        const { validateCallback, helperText } = validations;
        if (!validateCallback(value)) {
          setErrors({ ...errors, [key]: helperText });
        } else {
          setErrors({ ...errors, [key]: "" });
        }
      };

      const renderTypeOfComponent = ({type, label, key, variant, placeholder, validations, items = []}) => {
      
        switch (true) {
            case type === 'select':
               return <Select
                label={label}
                value={form[key]}
                onChange={(event) => handleChange(event, form[key])}
                input={<BootstrapInput />}
              >{items?.map((item) => <MenuItem value={item.value} label={item.label}>{item.text}</MenuItem>)}
              </Select>
              default: 
              return <TextField
              className="form__group field"
              {...{
                type,
                label,
                variant,
                placeholder,
                key: key,
                error: !!errors[key],
                value: form[key],
                helperText: errors[key] ? validations.helperText : null,
                onChange: (event) => handleChange(event, key),
                onBlur: (event) =>
                  validateInput(event.target.value, validations, key),
              }}
            />
              
      }
      }
    
      const formState = formConfig.map((component) => renderTypeOfComponent(component));

    return { formState, handleSubmit };
};

export default usePrepareForm;