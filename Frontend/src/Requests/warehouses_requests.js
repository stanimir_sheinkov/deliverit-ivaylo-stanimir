import axiosInstance from "../axios";

/**
 * GET ALL PARCELS
 * @returns {array} filled with warehouse info objects
 */
export const getWarehouses = async () => {
  const { data } = await axiosInstance.get("/warehouses");

  const result = data.map((item) => {
    return {
      id: item.idwarehouses,
      country: item.country,
      city: item.city,
      address: item.street,
    };
  });

  return result;
  //   try {
  //     const { status } = await axiosInstance.get("/warehouses");
  //     if (status === 201) {
  //         return successfulRegistration;
  //     }
  //   } catch (error) {
  //     if (error.response) {
  //       return error.message;
  //     }
  //   }
};

export const updateWarehouse = async (id, body) => {
  const { data } = await axiosInstance.put(`/warehouses/update/${id}`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

export const createWarehouse = async (body) => {
  const { data } = await axiosInstance.post(`/warehouses/create`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};
