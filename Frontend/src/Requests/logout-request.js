import { API_URL } from "../Common/constants";
import { getHeaders } from "../Common/constants";
export const logOutFromServer = () => {
    return fetch(`${API_URL}users/logout`, {
      method: 'DELETE',
      headers: getHeaders(),
    })
    .then(x => x.json())
    .catch(() => ({ message: 'Server have problem' }));
  }