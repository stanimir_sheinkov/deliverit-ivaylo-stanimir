import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import useRegisterPage from "../hooks/useRegisterPage";
import { Button, TextField } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: 200,
    },
  },
}));

const RegisterPage = () => {
  const classes = useStyles();
  const { formConfig, form, errors, handleInput, validateInput, handleSubmit } =
    useRegisterPage();

  return (
    <>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={(event) => handleSubmit(event, form)}
      >
        <div className="register-form">
          <h1 align="center">Sign Up</h1>
          {formConfig.map(
            ({ placeholder, type, label, key, variant, validations }) => (
              <TextField
                className="form__group field"
                {...{
                  type,
                  label,
                  variant,
                  placeholder,
                  key: key,
                  error: !!errors[key],
                  value: form[key],
                  helperText: errors[key] ? validations.helperText : null,
                  onChange: (event) => handleInput(event, key),
                  onBlur: (event) =>
                    validateInput(event.target.value, validations, key),
                }}
              />
            )
          )}
          <Button type="submit">Submit</Button>
        </div>
      </form>
    </>
  );
};

export default RegisterPage;
