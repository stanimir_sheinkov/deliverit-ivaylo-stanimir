// import React from "react";
import { Title } from "@material-ui/icons";
import { useState } from "react";
import * as constants from "../Common/constants";
import { registerUser } from "../services/AuthServices";

const formConfig = [
  {
    key: "first_name",
    placeholder: "First name",
    type: "text",
    label: "First name",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined", 
  },
  {
    key: "last_name",
    placeholder: "Last name",
    type: "text",
    label: "Last name",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "email",
    placeholder: "Email",
    type: "text",
    label: "Email",
    validations: {
      validateCallback: (value) => value.trim().length > 5 && value.length < 20,
      helperText: constants.emailErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "country",
    placeholder: "Country",
    type: "text",
    label: "Country",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "city",
    placeholder: "City",
    type: "text",
    label: "City",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "street",
    placeholder: "Street",
    type: "text",
    label: "Street",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "password",
    placeholder: "Password",
    type: "password",
    label: "Password",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.passwordErrorMessage,
    },
    variant: "outlined",
  },
];

const useRegisterPage = () => {
  const [form, setForm] = useState({
    first_name: "",
    last_name: "",
    email: "",
    country: "",
    city: "",
    street: "",
    password: "",
  });
  const [errors, setErrors] = useState({
    first_name: "",
    last_name: "",
    email: "",
    country: "",
    city: "",
    street: "",
    password: "",
  });

  const handleSubmit = (event, credentials) => {
    console.log(event);
    event.preventDefault();
    // const errorValues = Object.values(errors);
    // errorValues.filter((error) => !!error)
    // if (!!errorValues.length) {
    //   return;
    // }
    registerUser(credentials);
  };

  const handleInput = (event, key) => {
    setForm({ ...form, [key]: event.target.value });
    
  };

  const validateInput = (value, validations, key) => {
    const { validateCallback, helperText } = validations;
    if (!validateCallback(value)) {
      setErrors({ ...errors, [key]: helperText });
    } else {
      setErrors({ ...errors, [key]: "" });
    }
  };

  return { form, errors, formConfig, handleInput, validateInput, handleSubmit };
};

export default useRegisterPage;
