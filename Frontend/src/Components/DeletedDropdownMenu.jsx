// import React from "react";
// import { makeStyles } from "@material-ui/core/styles";
// // import InputLabel from "@material-ui/core/InputLabel";
// // import FormHelperText from "@material-ui/core/FormHelperText";
// import FormControl from "@material-ui/core/FormControl";
// import NativeSelect from "@material-ui/core/NativeSelect";

// const useStyles = makeStyles((theme) => ({
//   formControl: {
//     margin: theme.spacing(1),
//     minWidth: 120
//   },
//   selectEmpty: {
//     marginTop: theme.spacing(2)
//   }
// }));

// export default function DropDownDeleteMenu() {
//   const classes = useStyles();
//   const [state, setState] = React.useState({
//     is_deleted: "",

//   });

//   const handleChange = (event) => {
//     const state = event.target.value;
//     setState({
//       ...state,
//       [state]: event.target.value
//     });
//   };

//   return (
//     <div>
//       <FormControl className={classes.formControl} error>
//         {/* <InputLabel htmlFor="name-native-error">Deleted</InputLabel> */}
//         <NativeSelect
//         // value = {state.is_deleted}
//           onChange={handleChange}
//           name="name"
//           defaultValue={state.is_deleted}
//           inputProps={{
//             id: "name-native-error"
//           }}
//         >
//           <option value="1">Deleted</option>
//           <option value="0">Active</option>
//         </NativeSelect>
//         {/* <FormHelperText>Error</FormHelperText> */}
//       </FormControl>
//     </div>
//   );
// }