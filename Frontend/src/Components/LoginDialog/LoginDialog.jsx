import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
// import List from "@material-ui/core/List";
import AppBar from "@material-ui/core/AppBar";
// import Toolbar from "@material-ui/core/Toolbar";
// import IconButton from "@material-ui/core/IconButton";
// import Typography from "@material-ui/core/Typography";
// import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
// import { TextField } from "@material-ui/core";
import { Login } from "../Login/Login";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

export default function LoginDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button color="white" onClick={handleClickOpen}>
        Login
      </Button>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Login/>
          {/* <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Login
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              Login
            </Button>
          </Toolbar> */}
        </AppBar>
        {/* <List>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField id="outlined-basic" label="Email" variant="outlined" />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              id="outlined-basic"
              label="Password"
              variant="outlined"
            />
          </form>
        </List> */}
      </Dialog>
    </div>
  );
}
