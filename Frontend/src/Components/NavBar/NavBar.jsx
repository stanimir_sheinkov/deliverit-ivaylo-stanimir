import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { AuthContext, useAuthContext } from "../../Context/AuthContext";
import { logoutUser } from "../../Requests/user_requests";
// import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";
import TotalUserCount from "../TotalUserCount/TotalUserCount";
import { Route } from "react-router";
import { Redirect } from "react-router";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginLeft: theme.spacing(1),
    flexGrow: 1,
  },
}));

export default function ButtonAppBar() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  // const { user } = useAuthContext();
  const handleChange = (e) => {
    localStorage.removeItem('token');
    console.log("successful logout!")   
    window.location.reload(false);
    logoutUser(e);
  };

  
function unLoggedUser() {
  return (
    
      <AppBar style={{ backgroundColor: "#2e1534" }} position="flex">
        <Toolbar>
          <Typography variant="h5" className={classes.title} >
            DeliverIT
          </Typography>
          <TotalUserCount />
      <div className="nav-btn-group">
        <Link to="/warehouses" className="nav-btn">
          Warehouses
        </Link>
        </div>
        <div>
          <Link to="/users/login"><button className="auth-btn-group">Login</button></Link>
          <Link to="/users/register"><button className="auth-btn-group">Sign Up</button></Link>

          <Route path="/">
        <Redirect to="/warehouses"/>
      </Route>
        </div>
        </Toolbar>


      </AppBar>


  )
}
  function renderCustomerDisplaySwitch() {
    return(
    <AppBar style={{ backgroundColor: "#2e1534" }} position="flex">
      
        <Toolbar>
          <Typography variant="h5" className={classes.title} >
            DeliverIT
          </Typography>
    
      <div className="nav-btn-group">
        <Link to="/parcels_user" className="nav-btn">
          Parcels
        </Link>
        <Link to="/warehouses" className="nav-btn">
          Warehouses
        </Link>
        </div>
        <div>
          <button className="auth-btn-group" onClick={handleChange}>Logout</button>
        </div>
        </Toolbar>
      </AppBar>
    );
  }

  function renderEmployeeDisplaySwitch() {
    return(
      <AppBar style={{ backgroundColor: "#2e1534" }} position="flex">
      
      <Toolbar>
        <Typography variant="h5" className={classes.title} >
          DeliverIT
        </Typography>
      <div className="nav-btn-group">
        {/* {!!user.role &&  */}
        <Link to="/users" className="nav-btn">
          Users
        </Link>
        {/* } */}
        <Link to="/shipments" className="nav-btn">
          Shipments
        </Link>
        <Link to="/parcels" className="nav-btn">
          Parcels
        </Link>
        <Link to="/warehouses" className="nav-btn">
          Warehouses
        </Link>
        </div>
        <div>
          <button className="auth-btn-group" onClick={handleChange}>Logout</button>
        </div>
        </Toolbar>
      </AppBar>
    );
  }

  return (
    <div className={classes.root}>

          <div>
            {/* {!!user.role &&  */}
            <AuthContext.Consumer>{ authContext => (
              
              !authContext.user 
                ? (unLoggedUser()) 
                : (!authContext.user.role 
                    ? renderCustomerDisplaySwitch()
                    : renderEmployeeDisplaySwitch()
              ))}
            </AuthContext.Consumer>
              </div>

    </div>
  )
}


// AuthContext.Consumer>{ authContext => (
//   <BrowserRouter>
//   {/* <AuthContext.Provider> */}
//   <ButtonAppBar />
//   {/* <BasicButtonGroup /> */}
//   { !authContext.user 
//       ? (renderLoginSwitch()) 
//       : (!authContext.user.role 
//           ? renderCustomerSwitch()
//           : renderEmployeeSwitch())}
//   {/* </AuthContext.Provider> */}
// </BrowserRouter>
// )}
// </AuthContext.Consumer>
