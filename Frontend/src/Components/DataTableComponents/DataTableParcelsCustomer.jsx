import * as React from "react";
import { useState, useEffect } from "react";
import { DataGrid } from "@material-ui/data-grid";
import { getParcels, getUserParcels } from "../../Requests/parcels_requests";
// import AddParcelFields from "../AddParcelFields/AddParcelFields";
import { GridToolbar } from "@material-ui/data-grid";
// import EditParcelForm from "../EditForms/EditParcelForm";
// import userEvent from "@testing-library/user-event";
// import { AuthContext } from "../../Context/AuthContext";
// import { getUserDataFromToken } from "../../Common/token_functions";
import { AuthContext } from "../../Context/AuthContext";
import { useContext } from "react";
import EditParcelFormCustomer from "../EditForms/EditParcelFormCustomer";
import AddParcelFieldsCustomer from "../AddParcelFields/AddParcelFieldsCustomer";
import { customerTableParcels } from "../../Common/dataTableColumns";

// const columns = [
//   {
//     field: "",
//     headerName: "",
//     editable: false,
//     sortable: false,
//     filterable: false,
//     width: 140,
//     disableClickEventBubbling: true,
//     renderCell: (params) => {
//       return <EditParcelFormCustomer {...params.row} />;
//     },
//   },
//   { field: "id", headerName: "ID", width: 100 },
//   {
//     field: "default_idwarehouse",
//     headerName: "Warehouse",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "weight",
//     headerName: "Weight",
//     width: 130,
//     editable: false,
//   },
//   {
//     field: "category",
//     headerName: "Category",
//     width: 150,
//     editable: false,
//   },
//   // {
//   //   field: "iduser",
//   //   headerName: "User ID",
//   //   type: "number",
//   //   width: 130,
//   //   editable: false,
//   // },
//   {
//     field: "toWarehouse",
//     headerName: "To Address",
//     type: "boolean",
//     width: 170,
//     editable: false,
//   },
//   {
//     field: "idshipments",
//     headerName: "Shipment ID",
//     type: "number",
//     width: 160,
//     editable: false,
//   },
//   {
//     field: "shipment_status",
//     headerName: "Status",
//     width: 120,
//     editable: false,
//   },
//   {
//     field: "is_deleted",
//     headerName: "Deleted",
//     type: "boolean",
//     width: 130,
//     editable: false,
//   },
// ];

const columns = customerTableParcels;

export default function DataTableParcelsCustomer() {
  const [parcels, setParcels] = useState([]);
  const user = useContext(AuthContext);
  const id = user.user.id;
  useEffect((userId) => {
    getUserParcels(id).then((data) => setParcels(data));
  }, []);

 

  return (
    
    <div>
      <AddParcelFieldsCustomer />
    <br />
    <div style={{ height: "100%", width: "100%" }}>
      <DataGrid
        style={{ height: 500, width: "100%" }}
        rows={parcels}
        columns={columns}
        pageSize={5}
        // checkboxSelection
        disableSelectionOnClick
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
    </div>
  );
}
