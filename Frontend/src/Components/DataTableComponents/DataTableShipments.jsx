import * as React from "react";
import { DataGrid } from "@material-ui/data-grid";
import { useState, useEffect } from "react";
import { getShipments } from "../../Requests/shipments_requests";

import AddShipmentFields from "../AddShipmentFields/AddShipmentFields";
import { GridToolbar } from "@material-ui/data-grid";
// import { employeeTableShipments } from "../../Common/dataTableColumns";
// import EditShipmentForm from "../EditForms/EditShipmentForm";
import { employeeTableShipments } from "../../Common/dataTableColumns";


// const columns = [
//   {
//     field: "",
//     headerName: "",
//     editable: false,
//     sortable: false,
//     filterable: false,
//     width: 100,
//     disableClickEventBubbling: true,
//     renderCell: (params) => {
//       return <EditShipmentForm {...params.row} />;
//     },
//   },
//   { field: "id", headerName: "ID", width: 100 },
//   {
//     field: "status",
//     headerName: "Status",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "from_idwarehouses",
//     headerName: "From",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "to_idwarehouses",
//     headerName: "To",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "departure",
//     headerName: "Departure",
//     type: 'dateTime',
//     width: 200,
//     editable: false,
//   },
//   {
//     field: "arrival",
//     headerName: "Arrival",
//     type: 'dateTime',
//     width: 200,
//     editable: false,
//   },
//   {
//     field: "created_on",
//     headerName: "Created on",
//     type: 'dateTime',
//     width: 200,
//     editable: false,
//   },
// ];

const columns = employeeTableShipments;

export default function DataTableShipments() {
  const [shipments, setShipments] = useState([]);
  useEffect(() => {
    getShipments().then((data) => setShipments(data));
  }, []);

  return (
    <div>
      <AddShipmentFields />
    <br />
    <div style={{ height: "100%", width: "100%" }}>
      <DataGrid 
        style={{ height: 640, width: "100%" }}
        rows={shipments}
        columns={columns}
        pageSize={5}
        // checkboxSelection
        disableSelectionOnClick
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
    </div>
  );
}
