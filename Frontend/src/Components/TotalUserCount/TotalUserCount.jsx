import React from "react";
import { getUsers } from "../../Requests/user_requests";
import { useEffect, useState } from "react";

export default function TotalUserCount() {
  const [usersCount, setUserCount] = useState([]);
  useEffect(() => {
    getUsers().then((data) => setUserCount(data));
  }, []);

  return (
    <h3>
      {`There are currently ${usersCount.length} users using our services!`}
    </h3>
  );
}
