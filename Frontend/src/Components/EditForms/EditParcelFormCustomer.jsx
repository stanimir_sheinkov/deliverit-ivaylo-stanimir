import React from "react";
import Button from "@material-ui/core/Button";
// import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useState } from "react";

import MenuItem from "@material-ui/core/MenuItem";
// import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputBase from "@material-ui/core/InputBase";
import { withStyles } from "@material-ui/core/styles";
import { updateParcel } from "../../Requests/parcels_requests";
// import DropDownDeleteMenu from "../DeletedDropdownMenu";
import { Alert } from "@material-ui/lab";
// import ComboboxDelete from "../ComboboxDelete/ComboboxDelete";
// import Alert from '@material-ui/lab/Alert';

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

// const useStyles = makeStyles((theme) => ({
//   margin: {
//     margin: theme.spacing(1),
//   },
// }));

export default function EditParcelFormCustomer(props) {

  const [open, setOpen] = React.useState(false);
  const [form, setForm] = useState({
    default_idwarehouse: {value: props.default_idwarehouse, name: "default_idwarehouse"},
    weight: { value: props.weight, name: "weight" },
    category: { value: props.category, name: "category" },
    iduser: { value: props.iduser, name: "iduser" },
    to_warehouse: { value: props.to_warehouse, name: "to_warehouse" },
    idshipments: { value: props.idshipments, name: "idshipments" },
    is_deleted: { value: props.is_deleted, name: "is_deleted" },
  });
 

    // useEffect(() => {
    //   getParcels().then((data) => setFields(data));
    // }, []);

  const handleClickOpen = () => {
    if (props.shipment_status !== "preparing") {
      <Alert> Cannot edit a parcel that has already departed!</Alert>
      alert("Cannot edit a parcel that has already departed!")
    } else if (props.idshipments) {
        alert("Cannot edit a parcel that has already been assigned to a shipment!")
    }
      // return(
      // <div>
      // <Alert variant="filled" severity="error">
      // This is an error alert — check it out!
      // </Alert>
      // </div>)
     else (
    setOpen(true));
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    updateParcel(props.id, {
      default_idwarehouse: form.default_idwarehouse.value,
      weight: form.weight.value,
      category: form.category.value,
      iduser: form.iduser.value,
      to_warehouse: form.to_warehouse.value,
      idshipments: form.idshipments.value,
      is_deleted: form.is_deleted.value,
    });
  };

  const onChangeHandler = (event, key) => {

    const element = form[key];
    const newElement = { ...element, value: event.target.value };

    setForm({ ...form, [key]: newElement });
  };

  const handleEdit = (event) => {
    handleSubmit(event);
    handleClose();
  };
  console.log(form)
  return (
    <div>
      <form
        // className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Edit Parcel
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit Parcel</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Use the save button to accept the changes.
            </DialogContentText>
            {/* <TextField
              value={form.default_idwarehouse.value}
              onChange={(event) =>
                onChangeHandler(event, form.default_idwarehouse.name)
              }
              autoFocus
              margin="dense"
              label="Default WH"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.weight.value}
              onChange={(event) => onChangeHandler(event, form.weight.name)}
              label="Weight"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.category.value}
              onChange={(event) => onChangeHandler(event, form.category.name)}
              // label={props.city}
              // id="name"
              label="Category"
              // type="city"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.iduser.value}
              onChange={(event) => onChangeHandler(event, form.iduser.name)}
              // label={props.city}
              // id="name"
              label="UserID"
              // type="city"
              fullWidth
            /> */}
            
            {/* <TextField
              autoFocus
              margin="dense"
              value={form.to_warehouse.value}
              onChange={(event) =>
                onChangeHandler(event, form.to_warehouse.name)
              }
              // label={props.address}
              // id="name"
              label="To Address"
              // type="address"
              fullWidth
            /> */}
            {/* <TextField
              autoFocus
              margin="dense"
              value={form.idshipments.value}
              onChange={(event) =>
                onChangeHandler(event, form.idshipments.name)
              }
              // label={props.address}
              // id="name"
              label="ShipmentID"
              // type="address"
              fullWidth
            /> */}
            <Select
              label={"To Address"}
              id="demo-customized-select"
              value={form.to_warehouse.value}
              onChange={(event) => onChangeHandler(event, form.to_warehouse.name)}
              input={<BootstrapInput />}
            >
              <MenuItem value="1" label="To Address">
                To Address
              </MenuItem>
              <MenuItem value="0" label="To Warehouse">
                To Warehouse
              </MenuItem>
            </Select>
            <Select
              label={"Deleted"}
              id="demo-customized-select"
              value={form.is_deleted.value}
              onChange={(event) => onChangeHandler(event, form.is_deleted.name)}
              input={<BootstrapInput />}
            >
              <MenuItem value="1" label="Deleted">
                Deleted
              </MenuItem>
              <MenuItem value="0" label="Active">
                Active
              </MenuItem>
            </Select>
            {/* <Select
              label={"Deleted"}
              id="demo-customized-select"
              value={form.is_deleted}
              onChange={(event) => onChangeHandler(event, form.is_deleted.name)}
              input={<BootstrapInput />}
            >
              <MenuItem value="1" label="Deleted">
                Deleted
              </MenuItem>
              <MenuItem value="0" label="Active">
                Active
              </MenuItem>
            </Select> */}
            {/* <ComboboxDelete {...props}/> */}
            <br></br>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleEdit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </form>
    </div>
  );
}
