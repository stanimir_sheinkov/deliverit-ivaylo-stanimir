import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useState } from "react";

import MenuItem from "@material-ui/core/MenuItem";
// import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputBase from "@material-ui/core/InputBase";
import { withStyles } from "@material-ui/core/styles";
import { updateShipment } from "../../Requests/shipments_requests";

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

// const useStyles = makeStyles((theme) => ({
//   margin: {
//     margin: theme.spacing(1),
//   },
// }));

export default function EditShipmentForm(props) {
  const [open, setOpen] = React.useState(false);

  const [form, setForm] = useState({
    status: { value: props.status, name: "status" },
    from_idwarehouses: {
      value: props.from_idwarehouses,
      name: "from_idwarehouses",
    },
    to_idwarehouses: { value: props.to_idwarehouses, name: "to_idwarehouses" },
    departure: { value: props.departure, name: "departure" },
    arrival: { value: props.arrival, name: "arrival" },

    shipment_weight: { value: props.shipment_weight, name: "shipment_weight" }, // dobavih tova

    created_on: { value: props.created_on, name: "created_on" },
    is_deleted: { value: props.is_deleted, name: "is_deleted" },
  });
  //   useEffect(() => {
  //     getShipments().then((data) => setFields(data));
  //   }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    updateShipment(props.id, {
      status: form.status.value,
      from_idwarehouses: form.from_idwarehouses.value,
      to_idwarehouses: form.to_idwarehouses.value,
      departure: form.departure.value,
      arrival: form.arrival.value,
      shipment_weight: form.shipment_weight.value, // dobavih tova
      created_on: form.created_on.value,
      is_deleted: form.is_deleted.value,
      //  is_deleted: form.is_deleted.value
    });
  };
  const onChangeHandler = (event, key) => {
    const element = form[key];
    const newElement = { ...element, value: event.target.value };
    setForm({ ...form, [key]: newElement });
  };

  const handleEdit = (event) => {
    handleSubmit(event);
    handleClose();
  };
  return (
    <div>
      <form
        // className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Edit
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit shipment</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Use the save button to accept the changes.
            </DialogContentText>
            <TextField
              value={form.from_idwarehouses.value}
              onChange={(event) =>
                onChangeHandler(event, form.from_idwarehouses.name)
              }
              autoFocus
              margin="dense"
              // label={props.country}
              // id="name"
              label="From warehouse"
              // type="country"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.to_idwarehouses.value}
              onChange={(event) =>
                onChangeHandler(event, form.to_idwarehouses.name)
              }
              // label={props.city}
              // id="name"
              label="To warehouse"
              // type="city"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.departure.value}
              onChange={(event) => onChangeHandler(event, form.departure.name)}
              // label={props.address}
              // id="name"
              // label="Departure"
              type="datetime-local"
              // type="address"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.arrival.value}
              onChange={(event) => onChangeHandler(event, form.arrival.name)}
              // label={props.address}
              // id="name"
              // label="Arrival"
              type="datetime-local"
              // type="address"
              fullWidth
            />

            <Select
              label={"Deleted"}
              // id="demo-customized-select"
              value={form.is_deleted}
              onChange={(event) => onChangeHandler(event, form.is_deleted.name)}
              input={<BootstrapInput />}
            >
              <MenuItem value="1" label="Deleted">
                Deleted
              </MenuItem>
              <MenuItem value="0" label="Active">
                Active
              </MenuItem>
            </Select>
            <br />
            <Select
              labelId="demo-customized-select-label"
              // id="demo-customized-select"
              value={form.status}
              onChange={(event) => onChangeHandler(event, form.status.name)}
              input={<BootstrapInput />}
            >
              <MenuItem value="preparing">preparing</MenuItem>
              <MenuItem value="delivering">delivering</MenuItem>
              <MenuItem value="delivered">delivered</MenuItem>
            </Select>
            <br></br>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleEdit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </form>
    </div>
  );
}
