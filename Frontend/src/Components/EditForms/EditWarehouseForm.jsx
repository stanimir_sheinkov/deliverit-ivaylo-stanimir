import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useState } from "react";
import { updateWarehouse } from "../../Requests/warehouses_requests";

import MenuItem from "@material-ui/core/MenuItem";
// import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputBase from "@material-ui/core/InputBase";
import { withStyles } from "@material-ui/core/styles";

const BootstrapInput = withStyles((theme) => ({
  root: {
    "label + &": {
      marginTop: theme.spacing(3),
    },
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}))(InputBase);

// const useStyles = makeStyles((theme) => ({
//   margin: {
//     margin: theme.spacing(1),
//   },
// }));

export default function EditWarehouseForm(props) {
  const [open, setOpen] = React.useState(false);
  const [form, setForm] = useState({
    country: { value: props.country, name: "country" },
    city: { value: props.city, name: "city" },
    street: { value: props.address, name: "street" },
    is_deleted: { value: props.is_deleted, name: "is_deleted" },
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    updateWarehouse(props.id, {
      country: form.country.value,
      city: form.city.value,
      street: form.street.value,
      is_deleted: form.is_deleted.value,
    });
  };

  const onChangeHandler = (event, key) => {
    const element = form[key];
    const newElement = { ...element, value: event.target.value };
    setForm({ ...form, [key]: newElement });
  };

  const handleEdit = (event) => {
    handleSubmit(event);
    handleClose();
  };

  return (
    <div>
      <form
        // className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Edit
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Edit warehouse</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Use the save button to accept the changes.
            </DialogContentText>
            <TextField
              value={form.country.value}
              onChange={(event) => onChangeHandler(event, form.country.name)}
              autoFocus
              margin="dense"
              // label={props.country}
              // id="name"
              label="Country"
              // type="country"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.city.value}
              onChange={(event) => onChangeHandler(event, form.city.name)}
              // label={props.city}
              // id="name"
              label="City"
              // type="city"
              fullWidth
            />
            <TextField
              autoFocus
              margin="dense"
              value={form.street.value}
              onChange={(event) => onChangeHandler(event, form.street.name)}
              // label={props.address}
              // id="name"
              label="Address"
              // type="address"
              fullWidth
            />
            <Select
              label={"Deleted"}
              // id="demo-customized-select"
              value={form.is_deleted}
              onChange={(event) => onChangeHandler(event, "is_deleted")}
              input={<BootstrapInput />}
            >
              <MenuItem value="1" label="Deleted">
                Deleted
              </MenuItem>
              <MenuItem value="0" label="Active">
                Active
              </MenuItem>
            </Select>
            <br />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleEdit} color="primary">
              Save
            </Button>
          </DialogActions>
        </Dialog>
      </form>
    </div>
  );
}
